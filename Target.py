import PyQt5.QtGui
import math


class Target:
    """
    Représentation d'une cible
    """
    defaultColor: PyQt5.QtGui.QColor
    highlightCol: PyQt5.QtGui.QColor
    selectColor: PyQt5.QtGui.QColor
    x: int
    y: int
    size: int
    to_selected: bool
    highlighted: bool

    def __init__(self, x, y, size) -> None:
        self.defaultColor = PyQt5.QtGui.QColor(0, 0, 255)
        self.highlightColor = PyQt5.QtGui.QColor(0, 255, 0)
        self.selectColor = PyQt5.QtGui.QColor(255, 0, 0)
        self.tailles = {"small": 11, "big": 22}
        self.to_selected = False
        self.highlighted = False
        self.x = x
        self.y = y
        self.size = size

    def paint(self, painter: PyQt5.QtGui.QPainter, taille: str):
        """
        Paint la cible
        :param painter: L'outil PyQt5 qui permet de dessiner les cibles
        :param taille: Taille des cibles afficher
        """
        pen = PyQt5.QtGui.QPen()
        pen.setWidth(3)
        if self.to_selected:
            pen.setColor(self.selectColor)
            painter.setBrush(self.selectColor)
        elif self.highlighted:
            pen.setColor(self.highlightColor)
            painter.setBrush(self.highlightColor)
        else:
            pen.setColor(self.defaultColor)
            painter.setBrush(self.defaultColor)
        painter.setPen(pen)
        if taille == "small":
            self.size = 11
        elif taille == "big":
            self.size = 22
        painter.drawEllipse(self.x - int(self.size / 2), self.y - int(self.size / 2), self.size, self.size)

    def to_close(self, x, y, s):
        """
        Permet de définir si une cible supérpose une autre
        :param x: coordonée x de la cible 2
        :param y: coordonée y de la cible 2
        :param s: Distance minimum entre les deux cibles
        :return:
        """
        return (math.sqrt(math.pow((x - self.x), 2) + math.pow((y - self.y), 2)) - self.size / 2) < s
