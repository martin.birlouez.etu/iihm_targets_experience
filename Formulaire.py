from PyQt5.QtWidgets import QDialog, QHBoxLayout, QRadioButton, QPushButton, QVBoxLayout, QLabel, QLineEdit


class Furmulaire(QDialog):
    """
    Créé la boite de dialogue
    """
    def __init__(self, parent) -> None:
        super().__init__()
        self.mw = parent
        size = (400, 400)
        self.resize(size[0], size[1])
        self.btn_selected = None
        self.all_elements = QVBoxLayout(self)

        self.user_id_box = QHBoxLayout()
        user_id_label = QLabel("User Id :")
        self.user_id_ledit = QLineEdit()
        self.user_id_box.addWidget(user_id_label)
        self.user_id_box.addWidget(self.user_id_ledit)
        self.all_elements.addLayout(self.user_id_box)

        self.m_selector = QHBoxLayout()
        self.all_elements.addLayout(self.m_selector)

        b = QRadioButton("Bubble")
        b.toggled.connect(lambda: self.btnstate(b))
        b.setChecked(True)
        self.m_selector.addWidget(b)

        b = QRadioButton("Rope")
        b.toggled.connect(lambda: self.btnstate(b))
        self.m_selector.addWidget(b)

        b = QRadioButton("Normale")
        b.toggled.connect(lambda: self.btnstate(b))
        self.m_selector.addWidget(b)

        density_box = QHBoxLayout()
        density_label = QLabel("Density :")
        self.density_ledit = QLineEdit("0")
        density_box.addWidget(density_label)
        density_box.addWidget(self.density_ledit)
        self.all_elements.addLayout(density_box)

        tdc_box = QHBoxLayout()
        tdc_label = QLabel("Taille de cible :")
        self.tdc_ledit = QLineEdit("0")
        tdc_box.addWidget(tdc_label)
        tdc_box.addWidget(self.tdc_ledit)
        self.all_elements.addLayout(tdc_box)

        nbrep_box = QHBoxLayout()
        nbrep_label = QLabel("Nombre de repetition")
        self.nbrep_ledit = QLineEdit("0")
        nbrep_box.addWidget(nbrep_label)
        nbrep_box.addWidget(self.nbrep_ledit)
        self.all_elements.addLayout(nbrep_box)

        btn_validate = QPushButton("Validate")
        btn_validate.clicked.connect(self.launch_app)
        self.all_elements.addWidget(btn_validate)

    def launch_app(self):
        """
        Lance l'application aprés la complétion du formulaire
        """
        if self.btn_selected is None:
            print("You have not select methode")
        elif not self.user_id_ledit.text().isnumeric():
            print("Pick number for the userId")
        elif not self.nbrep_ledit.text().isnumeric():
            print("Pick number for the repetition number")
        elif not self.tdc_ledit.text().isnumeric():
            print("Pick number for target size number")
        elif not self.density_ledit.text().isnumeric():
            print("Pick number for density")
        args = {"userId": int(self.user_id_ledit.text()), "methode": self.btn_selected.text(),
                "density": int(self.density_ledit.text()), "tdc": int(self.tdc_ledit.text()),
                "rept": int(self.nbrep_ledit.text())}
        print(args)
        self.mw.launch_experience(args)

    def btnstate(self, b):
        """
        Get l'eta du Bouton selectionner
        :param b:
        """
        self.btn_selected = b
