import os
import csv
import operator


def sorte_line(f_log):
    """
    Permet de trier un fichier CSV selon 4 colonnes
    :param f_log: le nom/chemin du fichier a trier
    """
    data = csv.reader(open(f_log), delimiter=',')
    data = sorted(data, key=operator.itemgetter(4))
    data = sorted(data, key=operator.itemgetter(3))
    data = sorted(data, key=operator.itemgetter(2))
    data = sorted(data, key=operator.itemgetter(1))
    f = open(f_log, "w", encoding='UTF8', newline='')
    writer = csv.writer(f)
    writer.writerows(data)


d_log = "./log"
f_ls = [f for f in os.listdir(d_log) if os.path.isfile(os.path.join(d_log, f))]
for v in f_ls:
    print("file: ", v)
    sorte_line(os.path.join(d_log, v))
