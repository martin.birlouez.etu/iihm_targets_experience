# Script
import random as pif
import sys
from Target import Target


def target_ok(ts, ta):
    """
    Verifie si une cible ne supérpose pas une liste de cible
    :param ts: La liste des cibles
    :param ta: la cible
    :return: Bool si oui ou non il superpose la ccible avec la liste
    """
    for trc in ts:
        if trc.to_close(ta.x, ta.y, ta.size):
            return False
    return True


print("Script: Make Targets")
arguments = sys.argv[1:]
print(arguments)
# nbcible, taille espace
s = 1000
nbcible = int(arguments[0])
taille = int(arguments[1])
space = int(arguments[2])

# ALL TARGET
csv_all_target = open("./ressources/00_targets_all.csv", 'w')
allTarget = []
enought = True
cpt = 0

while enought:
    x, y = pif.randint(taille, s - taille), pif.randint(taille, s - taille)
    tc = Target(x, y, taille)
    if target_ok(allTarget, tc):
        csv_all_target.write(f"{x},{y},{taille}\r")
        allTarget.append(tc)
        cpt += 1
    if cpt >= nbcible:
        enought = False

print("Script: Gen All Targets : Ok")
csv_all_target.close()

# Faible densiter // haute densiter // pour Chaque méthode
# bb_d_t = pif.sample(range())

target_for_xp_hightdensity = pif.sample(range(0, nbcible), 90)
target_for_xp_lowdensity = pif.sample(range(0, nbcible), 40)
csv_hight_targets = open("./ressources/10_targets_hight_density.csv", "w")
for t in target_for_xp_hightdensity:
    csv_hight_targets.write(f"{t}\r")
csv_hight_targets.close()
csv_low_targets = open("./ressources/11_targets_low_density.csv", "w")
for t in target_for_xp_lowdensity:
    csv_low_targets.write(f"{t}\r")
csv_low_targets.close()

# On choisie que l'utilisateur devra viser 5 cibles
f_targets = ["./ressources/20_targets_to_select_bb.csv", "./ressources/21_targets_to_select_ro.csv",
             "./ressources/22_targets_to_select_nr.csv"]

for f_name in f_targets:
    toSelectedTargets_hight = pif.sample(target_for_xp_hightdensity, 5)
    toSelectedTargets_low = pif.sample(target_for_xp_lowdensity, 5)
    csv = open(f_name, "w")
    for t in toSelectedTargets_hight:
        csv.write(f"{t}\r")
    for t in toSelectedTargets_low:
        csv.write(f"{t}\r")
    csv.close()
