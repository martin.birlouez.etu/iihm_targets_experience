from PyQt5.QtGui import QPainter, QPen
from Target import Target
from XpWidget.absCursor import AbsCursor


class BubbleCursor(AbsCursor):
    """
    Curseur de la classe Bubble (affiche un cercle)
    """
    def __init__(self, targets) -> None:
        super().__init__(targets)
        self.name = "Bubble"

    def paint(self, painter: QPainter):
        """
        Permet de peindre l'ui du curseur (ici le cercle)
        :param painter: Painter permetant le dessin
        """
        pen = QPen()
        pen.setColor(self.defaultColor)
        pen.setWidth(2)
        painter.setPen(pen)
        painter.setBrush(self.defaultColor)
        painter.drawEllipse(self.x - self.size, self.y - self.size, self.size * 2, self.size * 2)

    def move(self, j: int, k: int):
        """
        Permet de modifier les états des cibles
        :param j: coordoné x sur le plan
        :param k: coordoné y sur le plan
        """
        self.x, self.y = j, k
        self.closest.highlighted = False
        temp_target: Target = self.targets[0]
        dist_tmp = self.distance(temp_target, j, k)
        for t in self.targets:
            dt = self.distance(t, j, k)
            if dt < dist_tmp:
                dist_tmp = dt
                temp_target = t
        self.closest = temp_target
        self.size = int(dist_tmp)
        self.closest.highlighted = True

    def distance(self, t: Target, x: int, y: int) -> float:
        """
        Calcule la distance entre le curseur et la cible
        :param t: Une cible avec ces coordonné x et y
        :param x: coordonée x de la 2eme position
        :param y: oordonée y de la 2eme position
        :return: la distance entre la cible et la position
        """
        return super().distance(t, x, y) - t.size / 2
