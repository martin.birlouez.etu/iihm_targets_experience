import math

from PyQt5.QtGui import QColor, QPainter

from Target import Target


class AbsCursor:
    """
    Classe abstraite qui représente un curseur
    """
    defaultColor: QColor
    x: int
    y: int
    size: int
    targets: []
    closest: Target

    def __init__(self, targets) -> None:
        self.defaultColor = QColor(0, 255, 255)
        self.x, self.y, self.size = 0, 0, 2048
        self.targets = targets
        if len(self.targets) < 3: return
        self.closest = targets[0]

    def paint(self, painter: QPainter):
        """
        fonction a implementer ppour paindre les Ui du cursor
        :param painter:
        """
        pass

    def move(self, i: int, j: int):
        """
        fonction a implementer pour le déplacement de la souris
        :param i: coordoné i sur le plan
        :param j: coordoné j sur le plan
        """
        pass

    def distance(self, t: Target, x: int, y: int) -> float:
        """
        Fonction de calcule des distances
        :param t: Une cible avec ces coordonné x et y
        :param x: coordonée x de la 2eme position
        :param y: coordonée y de la 2eme position
        :return: la distance entre la cible et la position
        """
        return math.sqrt(math.pow((x - t.x), 2) + math.pow((y - t.y), 2))
