import time
from PyQt5 import QtGui
from PyQt5.QtGui import QPainter
from PyQt5.QtWidgets import QWidget
from Target import Target
from XpWidget import absCursor


def read_target_file(f_targ):
    """
    Fonction permetant la lecture
    :param f_targ: le chemin du ficier contenant les cibles
    :return: une liste de cibles
    """
    # lecture csv
    csv = open(f_targ, 'r')
    data = csv.readlines()
    csv.close()
    targets = []
    for line in data:
        t = Target(int(line.split(',')[0]), int(line.split(',')[1]), int(line.split(',')[2]))
        targets.append(t)
    return targets


class AbsWidget(QWidget):
    """
    Classe abstraite représentant un widget d'interaction
    """
    name: str
    all_targets: [Target]
    target_to_select: [Target]
    cursor: absCursor
    nbError: int
    currTarget: int
    density: int
    taille: str

    def __init__(self, f_targets, f_targets_to_select, density, taille, xpman) -> None:
        super().__init__()
        self.xpman = xpman
        self.name = "default"
        self.density = density
        self.taille = taille
        # --- ! ! ! ! LE TRAKING BILLY ! ! ! ! ---
        self.setMouseTracking(True)
        self.all_targets = read_target_file(f_targets)
        targets_hight_density = self.read_idx_file_to_targets("./ressources/10_targets_hight_density.csv")
        targets_low_density = self.read_idx_file_to_targets("./ressources/11_targets_low_density.csv")
        self.targets_to_select = self.read_idx_file_to_targets(f_targets_to_select)
        if density == "low":
            self.targets_to_use = targets_low_density
            self.targets_to_select = self.targets_to_select[5:]
        elif density == "hight":
            self.targets_to_use = targets_hight_density
            self.targets_to_select = self.targets_to_select[:5]
        self.nbError = 0
        self.curr_targets = -1
        self.timestart = None
        self.next_target()

    def read_idx_file_to_targets(self, f_idx):
        """
        Lit un fichier d'indexe de cible fesant référence a la liste complete des cibles
        :param f_idx: chemin du fichier d'indexe
        :return: renvoie une liste de cibles
        """
        csv = open(f_idx, 'r')
        data = csv.readlines()
        csv.close()
        targets = []
        for line in data:
            t = self.all_targets[int(line)]
            targets.append(t)
        return targets

    def mouseMoveEvent(self, a0: QtGui.QMouseEvent) -> None:
        """
        overide methode de pyQt
        :param a0: L'event
        """
        self.cursor.move(a0.x(), a0.y())
        self.update()

    def paintEvent(self, a0: QtGui.QPaintEvent) -> None:
        """
        overide methode de pyQt
        :param a0: L'event
        """
        painter = QPainter(self)
        self.cursor.paint(painter)
        if self.density == "low" or self.density == "hight":
            for t in self.targets_to_use:
                t.paint(painter, self.taille)
        else:
            for t in self.all_targets:
                t.paint(painter, self.taille)

    def next_target(self):
        """
        Permet de passer a la cible suivante
        """
        self.targets_to_select[self.curr_targets].to_selected = False
        self.curr_targets += 1
        self.targets_to_select[self.curr_targets].to_selected = True
        self.nbError = 0
        self.start_chrono()

    def target_done(self):
        """
        Quand la cible est seletionner appele cette méthode
        """
        time_travel = time.time() - self.timestart
        print(f"Temps: {time_travel}")  # a voir comment on affiche sa bien aprés
        self.xpman.write_in_log(self.nbError, time_travel)
        if self.curr_targets >= len(self.targets_to_select) - 1:
            self.xpman.next_round()
        else:
            self.next_target()

    def restart(self):
        """
        Réinitialise certain paramétre
        """
        self.nbError = 0
        self.targets_to_select[self.curr_targets].to_selected = False
        self.curr_targets = -1
        self.next_target()

    def start_chrono(self):
        """
        Démare un cronométre
        """
        self.timestart = time.time()
