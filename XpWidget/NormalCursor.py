from PyQt5.QtGui import QPainter
from XpWidget.absCursor import AbsCursor


class NormalCursor(AbsCursor):
    """
    Curseur de la classe Bubble (affiche rien)
    """
    def __init__(self, targets) -> None:
        super().__init__(targets)

    def paint(self, painter: QPainter):
        """
        Do nothing
        :param painter: not use
        """
        pass

    def move(self, j: int, k: int):
        """
        Permet de modifier les états des cibles
        :param j: coordoné x sur le plan
        :param k: coordoné y sur le plan
        """
        self.x, self.y = j, k
        if self.closest is not None:
            self.closest.highlighted = False
            # noinspection PyTypeChecker
            self.closest = None
        for t in self.targets:
            if self.distance(t, j, k) < t.size / 2:
                self.closest = t
                self.closest.highlighted = True
                break

