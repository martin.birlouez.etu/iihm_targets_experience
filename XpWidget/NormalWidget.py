from PyQt5 import QtGui

from XpWidget.NormalCursor import NormalCursor
from XpWidget.absWidget import AbsWidget


class NormalWidget(AbsWidget):
    """
    La classe du widget Normale
    """
    def __init__(self, f_targets, f_targets_to_select, density, taille, xpman) -> None:
        super().__init__(f_targets, f_targets_to_select, density, taille, xpman)
        self.name = "Normale"
        self.cursor = NormalCursor(self.targets_to_use)

    def mousePressEvent(self, a0: QtGui.QMouseEvent) -> None:
        """
        overide de la méthode mousePressEvent
        :param a0: L'event
        """
        if self.cursor.closest is not None and self.cursor.closest.to_selected and self.cursor.closest.highlighted:
            self.target_done()
        else:
            print("Bad Selection")
            self.nbError += 1
        self.update()
