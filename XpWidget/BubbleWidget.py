from PyQt5 import QtGui
from XpWidget.BubbleCursor import BubbleCursor
from XpWidget.absWidget import AbsWidget


class BubbleWidget(AbsWidget):
    """
    La classe du widget Bubble
    """
    def __init__(self, f_targets, f_targets_to_select, density, taille, xpman) -> None:
        super().__init__(f_targets, f_targets_to_select, density, taille, xpman)
        self.name = "Bubble"
        self.cursor = BubbleCursor(self.targets_to_use)

    def mousePressEvent(self, a0: QtGui.QMouseEvent) -> None:
        """
        overide de la méthode mousePressEvent
        :param a0: L'event
        """
        if self.cursor.closest.to_selected:
            self.target_done()
        else:
            print("Bad Selection")
            self.nbError += 1
        self.update()
