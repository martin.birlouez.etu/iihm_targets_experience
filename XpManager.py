import time
import random as pif
from XpWidget.BubbleWidget import BubbleWidget
from XpWidget.NormalWidget import NormalWidget
from XpWidget.RopeWidget import RopeWidget
from Scripts.csvSorter import sorte_line


class RoundXp:
    """
    Représente un Round pour l'experience (avec ces paramétres)
    """
    taille: str
    density: str
    methode: str

    def __init__(self, taille, density, methode) -> None:
        self.taille = taille
        self.density = density
        self.methode = methode


class XpManager:
    """
    Sert a manager les différents paramétres et lance les différents Rounds
    """
    def __init__(self, userid, mw) -> None:
        self.taille = ["small", "big"]  # 11 - 22
        self.density = ["low", "hight"]  # 40 - 90
        self.methodes = ["Bubble", "Rope", "Normale"]
        self.repetition = 3
        self.occ = 0
        self.userid = userid
        self.methode = None
        self.mw = mw
        self.f_targets = "./ressources/00_targets_all.csv"
        self.f_targets_to_select_bb = "./ressources/20_targets_to_select_bb.csv"
        self.f_targets_to_select_ro = "./ressources/21_targets_to_select_ro.csv"
        self.f_targets_to_select_nr = "./ressources/22_targets_to_select_nr.csv"
        # Log File
        self.f_log = f"./log/log_{userid}_{time.ctime(time.time()).replace(' ', '_').replace(':', '-')}.csv"
        log = open(self.f_log, "w")
        log.close()
        # init sequencage de l'experience
        pif.shuffle(self.taille)
        pif.shuffle(self.density)
        pif.shuffle(self.methodes)
        self.idx_seq = 0
        self.sequenceur = []
        for m in self.methodes:
            for t in self.taille:
                for d in self.density:
                    sxp = RoundXp(t, d, m)
                    self.sequenceur.append(sxp)
        pif.shuffle(self.sequenceur)

    def configure_round(self, s: RoundXp):
        """
        Permet de configuré un des round de l'experience avec ces différent paramétres
        :param s: Prend la cnfiguration d'un Rounds
        """
        m = s.methode
        if m == "Bubble":
            self.methode = BubbleWidget(self.f_targets, self.f_targets_to_select_bb, s.density, s.taille, self)
        elif m == "Rope":
            self.methode = RopeWidget(self.f_targets, self.f_targets_to_select_ro, s.density, s.taille, self)
        elif m == "Normale":
            self.methode = NormalWidget(self.f_targets, self.f_targets_to_select_nr, s.density, s.taille, self)

    def launch(self):
        """
        Lance une des configurations de la séquence
        """
        print(f"seq = {self.idx_seq}")
        s = self.sequenceur[self.idx_seq]
        self.configure_round(s)
        self.mw.setCentralWidget(self.methode)
        self.mw.show()

    def write_in_log(self, nb_error, time_to_select):
        """
        Ecrit dans le fichier log de l'esperience manager
        :param nb_error: le nombre d'erreur que l'utilisateur
        :param time_to_select: Temps mis a la seection d'une cible
        """
        # user id, methode, taille, density, repetition, error, time
        s = self.sequenceur[self.idx_seq]
        log = open(self.f_log, "a")
        log.write(f"{self.userid},{self.methode.name},{s.taille},{s.density},"
                  f"{self.occ},{nb_error},{str(time_to_select)[:6]}\r")
        log.close()

    def next_round(self):
        """
        Lance le prochain Round
        """
        self.idx_seq += 1
        if self.idx_seq >= len(self.sequenceur):
            self.idx_seq = 0
            self.occ += 1
        if self.occ >= self.repetition:
            self.end_xp()
        self.launch()

    def end_xp(self):
        """
        Met fin a l'experience t qui l'app
        """
        sorte_line(self.f_log)
        exit(0)
