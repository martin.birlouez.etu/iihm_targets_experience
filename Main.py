import sys
from PyQt5.QtWidgets import QMainWindow, QApplication
from Formulaire import Furmulaire
from XpManager import XpManager


class MainWindows(QMainWindow):
    """
    Class de l'application principale
    """

    def __init__(self) -> None:
        super().__init__()
        self.resize(1000, 1000)
        self.setup = Furmulaire(self)
        self.setup.show()

    def launch_experience(self, argss):
        """
        Permet de lancer l'experience
        :param argss: liste des arguments passer par la boite de Dialogue
        """
        self.setup.close()
        xpman = XpManager(argss["userId"], self)
        xpman.launch()


if __name__ == "__main__":
    print("Log: Xp launch")
    args = sys.argv[1:]
    app = QApplication(sys.argv)
    window = MainWindows()
    app.exec()
